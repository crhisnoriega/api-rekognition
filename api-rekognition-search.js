const aws = require('aws-sdk');
const fs = require("fs");

const s3 = new aws.S3();
const rekognition = new aws.Rekognition();

const Sequelize = require("sequelize");

const sequelize = new Sequelize(
    "postgres://postgres:start2020@vpsw0811.publiccloud.com.br:5432/bancoImagem", {
        quoteIdentifiers: false
    }
);
const crypto = require('crypto');

exports.handler = (event, context, callback) => {
    var params = JSON.parse(event.body);

    context.callbackWaitsForEmptyEventLoop = false;

    put("api-rekognition", "to_search.jpg", new Buffer(params.content, "base64")).then(result_s3 => {
        searchFaceS3Object("to_search.jpg").then(result => {
            if (result.FaceMatches.length > 0) {
                var response = {
                    "statusCode": 200,
                    "body": JSON.stringify(result),
                    "isBase64Encoded": false
                };
                callback(null, response);

            }
            else {
                var response = {
                    "statusCode": 200,
                    "body": JSON.stringify("empty result 3"),
                    "isBase64Encoded": false
                };
                callback(null, response);
            }
        }).catch(err => {
            var response = {
                "statusCode": 500,
                "body": JSON.stringify(err),
                "isBase64Encoded": false
            };
            callback(null, response);
        });
    });
};



function indexFaceBytes(buffer) {
    return new Promise((resolve, reject) => {
        rekognition.indexFaces({
            CollectionId: "grx-faces",
            DetectionAttributes: [
                "ALL"
            ],
            Image: {
                Bytes: buffer
            }
        }, (err, data) => {
            if (err) {
                return reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}


function put(destBucket, destKey, data) {
    return new Promise((resolve, reject) => {
        s3.putObject({
            Bucket: 'api-rekognition',
            Key: destKey,
            Body: data
        }, (err, data) => {
            if (err) {
                console.error('Error putting object: ' + destBucket + ':' + destKey);
                return reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}


function indexFaceS3Object(keyS3) {
    return new Promise((resolve, reject) => {
        rekognition.indexFaces({
            CollectionId: "grx-faces",
            DetectionAttributes: [
                "ALL"
            ],
            Image: {
                S3Object: {
                    Bucket: "api-rekognition",
                    Name: keyS3
                }
            },
            MaxFaces: 5
        }, (err, data) => {
            if (err) {
                return reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}

function searchFaceS3Object(keyS3) {
    return new Promise((resolve, reject) => {
        rekognition.searchFacesByImage({
            CollectionId: "grx-faces",
            Image: {
                S3Object: {
                    Bucket: "api-rekognition",
                    Name: keyS3
                }
            },
            MaxFaces: 5
        }, (err, data) => {
            if (err) {
                return reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}

function searchFaceBytes(buffer) {
    return new Promise((resolve, reject) => {
        rekognition.searchFacesByImage({
            CollectionId: "grx-faces",
            Image: {
                Bytes: buffer
            },
            MaxFaces: 5
        }, (err, data) => {
            if (err) {
                return reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}
