const aws = require('aws-sdk');
const fs = require("fs");

const s3 = new aws.S3();
const rekognition = new aws.Rekognition();

const Sequelize = require("sequelize");

const sequelize = new Sequelize(
    "postgres://postgres:start2020@vpsw0811.publiccloud.com.br:5432/bancoImagem", {
        quoteIdentifiers: false
    }
);
const crypto = require('crypto');

exports.handler = (event, context, callback) => {
    var params = JSON.parse(event.body);


    context.callbackWaitsForEmptyEventLoop = false;
    var sql = 'SELECT * from imagens where id = ' + params.id;

    sequelize.query(sql, { type: sequelize.QueryTypes.SELECT })
        .then(result_sql => {
            var buff = result_sql[0].imagem;
            put("api-rekognition", result_sql[0].nome, buff).then(result_s3 => {
                console.log(result_s3);
                indexFaceS3Object(result_sql[0].nome).then(result_rekog => {
                    var insert_sql = "INSERT INTO public.facesids(face_id, user_id, user_name) VALUES ('" + result_rekog.FaceRecords[0].Face.FaceId + "', " + result_sql[0].id + ",'" + result_sql[0].nome + "');"
                    sequelize.query(insert_sql).then(r => {
                        var response = {
                            "statusCode": 200,
                            "body": JSON.stringify(result_rekog),
                            "isBase64Encoded": false
                        };
                        callback(null, response);
                    });

                })
            });
        })
        .catch(error => {
            callback(Error(error));
        });

};



function indexFaceBytes(buffer) {
    return new Promise((resolve, reject) => {
        rekognition.indexFaces({
            CollectionId: "grx-faces",
            DetectionAttributes: [
                "ALL"
            ],
            Image: {
                Bytes: buffer
            }
        }, (err, data) => {
            if (err) {
                return reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}


function put(destBucket, destKey, data) {
    return new Promise((resolve, reject) => {
        s3.putObject({
            Bucket: 'api-rekognition',
            Key: destKey,
            Body: data
        }, (err, data) => {
            if (err) {
                console.error('Error putting object: ' + destBucket + ':' + destKey);
                return reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}


function indexFaceS3Object(keyS3) {
    return new Promise((resolve, reject) => {
        rekognition.indexFaces({
            CollectionId: "grx-faces",
            DetectionAttributes: [
                "ALL"
            ],
            Image: {
                S3Object: {
                    Bucket: "api-rekognition",
                    Name: keyS3
                }
            },
            MaxFaces: 5
        }, (err, data) => {
            if (err) {
                return reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}

function searchFaceS3Object(keyS3) {
    return new Promise((resolve, reject) => {
        rekognition.searchFacesByImage({
            CollectionId: "grx-faces",
            Image: {
                S3Object: {
                    Bucket: "api-rekognition",
                    Name: keyS3
                }
            },
            MaxFaces: 5
        }, (err, data) => {
            if (err) {
                return reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}

function searchFaceBytes(buffer) {
    return new Promise((resolve, reject) => {
        rekognition.searchFacesByImage({
            CollectionId: "grx-faces",
            Image: {
                Bytes: buffer
            },
            MaxFaces: 5
        }, (err, data) => {
            if (err) {
                return reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}
